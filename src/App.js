import React, { Component } from "react";
import Button from "./components/button";
import Modal from "./components/modal";
import { AppContainer, GlobalStyle } from "./styles/style.js";

class App extends Component {
  constructor() {
    super();

    this.state = {
      modalIsVisible1: false,
      modalIsVisible2: false,
    };
  }

  render() {
    return (
      <AppContainer>
        <GlobalStyle />
        <Button
          className={"green"}
          text="Open first modal"
          backgroundColor="#b3382c"
          onClick={() =>
            this.setState({
              modalIsVisible1: true,
            })
          }
        />{" "}
        {this.state.modalIsVisible1 && (
          <Modal
            className={"red"}
            setVisible={() =>
              this.setState({
                modalIsVisible1: false,
              })
            }
            header={<> Do you want to delete this file ? </>}
            closeButton={true}
            text={
              <>
                <p>
                  {" "}
                  Once you delete this file, it won 't be possible to undo this
                  action.
                </p>{" "}
                <p> Are you sure you want to delete it ? </p>{" "}
              </>
            }
            action={
              <>
                <Button
                  className={"red"}
                  text="OK"
                  backgroundColor="#b3382c"
                  onClick={() => alert("OK")}
                />{" "}
                <Button
                  className={"red"}
                  text="Cancel"
                  backgroundColor="#b3382c"
                  onClick={() =>
                    this.setState({
                      modalIsVisible1: false,
                    })
                  }
                />{" "}
              </>
            }
          />
        )}
        <Button
          className={"blue"}
          text="Open second modal"
          backgroundColor="#5661e1"
          onClick={() =>
            this.setState({
              modalIsVisible2: true,
            })
          }
        />{" "}
        {this.state.modalIsVisible2 && (
          <Modal
            className={"blue"}
            setVisible={() =>
              this.setState({
                modalIsVisible2: false,
              })
            }
            header={<> Second modal window </>}
            closeButton={false}
            text={
              <>
                <p> This is a second modal window for test. </p>{" "}
                <p> It 's working!</p>{" "}
              </>
            }
            action={
              <>
                <Button
                  className={"green"}
                  text="Cancel"
                  backgroundColor="#34ab40"
                  onClick={() =>
                    this.setState({
                      modalIsVisible2: false,
                    })
                  }
                />{" "}
              </>
            }
          />
        )}{" "}
      </AppContainer>
    );
  }
}

export default App;
