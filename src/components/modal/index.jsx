import React, { Component } from "react";
import {
  MyModalWrapper,
  MyModal,
  MyModalHeader,
  MyModalCloseBtn,
  MyModalContent,
  MyModalAction,
} from "../../styles/modal";

class Modal extends Component {
  render() {
    const { header, closeButton, text, action, className, setVisible } =
      this.props;

    return (
      <MyModalWrapper onClick={setVisible}>
        <MyModal className={className} onClick={(e) => e.stopPropagation}>
          <MyModalHeader className={className}>
            <h1>{header}</h1>
            {closeButton && (
              <MyModalCloseBtn onClick={setVisible}>X</MyModalCloseBtn>
            )}
          </MyModalHeader>
          <MyModalContent>{text}</MyModalContent>
          <MyModalAction>{action}</MyModalAction>
        </MyModal>
      </MyModalWrapper>
    );
  }
}

export default Modal;
