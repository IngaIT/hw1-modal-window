import React, { Component } from "react";
import { MyButton } from "../../styles/button.js";

class Button extends Component {
  render() {
    const { className, backgroundColor, onClick, text } = this.props;

    return (
      <MyButton
        className={className}
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >
        {text}
      </MyButton>
    );
  }
}

export default Button;
